import "./components/orxe-button";

import { customElement, LitElement, html } from "lit-element";

@customElement("root-el")
export class RootElement extends LitElement {
  render() {
    
    return html`
    
    <form @submit="${this.handleclick}" method="get">
    First name: <input type="text" ><br>
    Last name: <input type="text" ><br>
    <orxe-button type = "reset" colorType="tertiary" value="Reset"></orxe-button>
    <orxe-button type = "submit" colorType="primary" type value="Submit"></orxe-button>
    <orxe-button iconPath="https://i.ibb.co/t2kCnBy/sign-check-flat-icon-png-29.png" variation="textandicon" colorType="danger"  value="buttonWithIcon"></orxe-button>
    <orxe-button  iconPath="https://i.ibb.co/t2kCnBy/sign-check-flat-icon-png-29.png" variation="icononly" colorType="ghost" value="buttonWithIcon"></orxe-button>
    <orxe-button tooltip="checkmark icon" href="/home" size="large" colorType="ghost" value="NEXT"></orxe-button>
    </form> `;
  
  }
  handleclick(e:CustomEvent){
    e.preventDefault();
    e.stopPropagation();
    console.log(e)
  }
  
  createRenderRoot() {
    return this;
  }
}
