import { html, LitElement, customElement, property } from "lit-element";
import orxeButtonScss from './orxe-button.scss';
import { ifDefined } from 'lit-html/directives/if-defined';

@customElement("orxe-button")
export default class OrxeButton extends LitElement {
    /**
     * `true` if the button should be disabled.
     */
    @property({ type: Boolean })
    disabled = false;
   /**
   * The color to use from your application's color palette.
   * Default options are: `"primary"` , `"secondary"`, `"tertiary"`,  `"danger"`, `"ghost"`.
   */
    @property({ type: String })
    colorType : "primary" |"secondary" | "tertiary" | "danger" | "ghost"= 'primary';
    /**
     *  set text value of button . 
     */
    @property({ type: String })
    value = 'click here';
    /**
       *  set type of button  . 
       */
    @property({ type: String })
    variation : "textAndIcon" | "iconOnly" | "textOnly"= 'textOnly';
    /** 
       *  set icon path for icon button. 
       */
    @property({ type: String })
    iconPath = null;
    /**
   * The default file name, used if this button is rendered as `<a>`.
   */
    @property({ type: String })
    download = undefined;

    /**
     * Link `href`. Corresponds to the attribute with the same name. If present, this button is rendered as `<a>`.
     */
    @property({ type: String })
    href = "";

    /**
     * The language of what `href` points to, if this button is rendered as `<a>`.
     * @type {[type]}
     */
    @property({ type: String })
    hreflang = "";

    /**
    * URLs to ping, if this button is rendered as `<a>`.
    */
    @property({ type: String })
    ping ="";

    /**
     * The link type, if this button is rendered as `<a>`. 
     */
    @property({ type: String })
    rel = "";


    /**
     * `true` if the button should have input focus when the page loads. 
     */
    @property({ type: Boolean, reflect: true })
    autofocus = false;

    /**
   * The link target, if this button is rendered as `<a>`. 
   */
    @property({ type: String })
    target = "_self";

    /**
     * The default behavior if the button is rendered as  /**
     * Type of button
     * Possible values are: `"submit"`, `"reset"`, `"button"`.
     *`<button>` . MIME type of the `target`if this button is rendered as `<a>`.
     */
    @property({ type: String })
    type = "";

       /**
   * Set icon position.
   * Default options are: `"right"` and `"left"`.
   */
    @property({ type: String })
    position = "left";
  
    /**
    * Set button size.
    * Default options are: `"small"` ,`"medium"`,`"arge"`.
    */
    @property({ type: String })
    size = "small";

    /**
   * Set icon tooltip.
   * when button variation attribute value 'iconOnly'.
   */
    @property({ type: String })
    tooltip = null;

    constructor() {
        super();
    }

    connectedCallback() {
        super.connectedCallback();

    }

    firstUpdated() {

    }

    renderButton() {
        let buttonType = this.variation.toLowerCase();
        let position = this.position.toLowerCase()
        if (buttonType === 'textandicon' && this.iconPath != null) {
            return html`
            ${position === "right" ?  this.value : '' } 
            <img class="icon" src="${this.iconPath}"
            alt="image" border="0" 
            >
            ${position === "left" ?  this.value : '' } 
          
            `
        } else if (buttonType === 'icononly' ) {
            return html`
            ${this.tooltip != null ? html`<span class="bx--assistive-text" aria-label="${this.tooltip}">${this.tooltip}</span>` : null }
            <img  class="icon" src="${this.iconPath}"
            alt="image" border="0" 
            >`
        } else {
            return this.value
        }
    }
    handleClick(event: MouseEvent) {
        if (this.disabled) {
            event.preventDefault();
            event.stopPropagation();
        }
        let clickEvent = new Event('click', { bubbles: true, composed: true });
        this.dispatchEvent(clickEvent);
    }

    render() {
        const { autofocus, disabled, download, href, hreflang, ping, rel, target, type } = this;
        let btnClass = this.colorType.toLowerCase();
        let iconOnly = 'bx--btn--icon-only bx--tooltip__trigger bx--tooltip--a11y bx--tooltip--bottom bx--tooltip--align-center bx--btn--sm';
        return href? html`
        <a
        id="button"
        role="button"   
        class="bx--link c-btn--${this.size}"
        href="${ifDefined(href)}"
        download="${ifDefined(download)}"
        hreflang="${ ifDefined(hreflang)}"
        ping="${ ifDefined(ping)}"
        rel="${ifDefined(rel)}"
        target="${ ifDefined(target)}"
        type="${ifDefined(type)}"
        @click="${this.handleClick}}">
        ${this.value}</a>`
        : html`
        <button @click = "${this.handleClick}" type=${type} 
        class="orxe-button bx--btn bx--btn--${btnClass} c-btn--${this.size} 
        ${ this.tooltip ? iconOnly : ''}"
        ?autofocus="${autofocus}" ?disabled="${disabled}" >
        ${this.renderButton()}
        </button> `
    }

    createRenderRoot() {
        return this;
    }

    static styles = orxeButtonScss;
}
